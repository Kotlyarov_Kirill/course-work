#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netdb.h> 
#include <fcntl.h>

#define str1 "Запрос обработан\r\n" 

void error(const char *msg) {
    perror(msg);
    exit(0);
}

int main() {
    int sock, my_sock, n;
    struct sockaddr_in addr;
    struct sockaddr_in serv_addr;
    char buf[1024];
    char bufer[1024];
    int bytes_read;
    int chislo = 1 + rand() % 10;
    //udp
    sock = socket(AF_INET, SOCK_DGRAM, 0);
    fcntl(sock, F_SETFL, O_NONBLOCK);
    if (sock < 0) {
        perror("socket");
        exit(1);
    }

    addr.sin_family = AF_INET;
    addr.sin_port = htons(3004);
    addr.sin_addr.s_addr = htonl(INADDR_ANY);
    /*
    if (bind(sock, (struct sockaddr *) &addr, sizeof (addr)) < 0) {
        perror("bind");
        exit(2);
    }
    */
    bind(sock, (struct sockaddr *) &addr, sizeof (addr));
    

    //tcp
    my_sock = socket(AF_INET, SOCK_STREAM, 0);
    if (my_sock < 0) {
        perror("socket");
        exit(1);
    }

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(3002); // или любой другой порт...
    serv_addr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
/*
    if (connect(my_sock, (struct sockaddr *) &serv_addr, sizeof (serv_addr)) < 0) {
        perror("connect");
        exit(2);
    }
*/    
    connect(my_sock, (struct sockaddr *) &serv_addr, sizeof (serv_addr));
    
    while (1) {
        bytes_read = recvfrom(sock, buf, 1024, 0, NULL, NULL); //Принемаем от сервера сообщение udp
        if (bytes_read > 0) {
            buf[bytes_read] = '\0'; //ставим завершающий ноль в конце строки
            printf("%s\n", buf);
            n = recv(my_sock, bufer, sizeof (bufer), 0);
            bufer[n] = 0; // ставим завершающий ноль в конце строки
            printf("\n S=>C:%s", bufer);
            write(my_sock, str1, strlen(str1)); // отправляем серверу результат tcp
        }
        
        sleep(chislo);
        //close(my_sock);
        //connect(my_sock, (struct sockaddr *) &serv_addr, sizeof (serv_addr));
    }
    printf("Recv error \n");
    close(my_sock);
    return -1;
}
