#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netdb.h> 

#define str1 "Запрос обработан 1\r\n" 

void error(const char *msg) {
    perror(msg);
    exit(0);
}

int main() {
    int sock, my_sock;
    struct sockaddr_in addr;
    struct sockaddr_in serv_addr;
    char buf[1024];
    int bytes_read;
    //int chislo = 1 + rand() %10;
    //Рандомная строка
    char symb[52] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    int p, k; // рандомные переменные

    //		printf("rand %d\n", p);

    int i;

    //udp
    sock = socket(AF_INET, SOCK_DGRAM, 0);
    if (sock < 0) {
        perror("socket");
        exit(1);
    }

    addr.sin_family = AF_INET;
    addr.sin_port = htons(3003);
    addr.sin_addr.s_addr = htonl(INADDR_ANY);
    bind(sock, (struct sockaddr *) &addr, sizeof (addr));

    //tcp

    my_sock = socket(AF_INET, SOCK_STREAM, 0);
    if (my_sock < 0) {
        perror("socket");
        exit(1);
    }

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(3001); // или любой другой порт...
    serv_addr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
    if (connect(my_sock, (struct sockaddr *) &serv_addr, sizeof (serv_addr)) < 0) {
        perror("connect");
        exit(2);
    }
  
    while (1) {
        bytes_read = recvfrom(sock, buf, 1024, 0, NULL, NULL); //Принемаем от сервера сообщение udp
        buf[bytes_read] = '\0'; // ставим завершающий ноль в конце строки
        printf("%s\n", buf);
        if (bytes_read > 0) {
            //Создаем рандомное сообщение рандомной длины
            p = rand() % 52;
            char bufer_symb[p];
            for (i = 0; i < p; i++) {
                k = rand() % 52;
                bufer_symb[i] = symb[k];
            }
            bufer_symb[p] = '\0'; // ставим завершающий ноль в конце строки
            printf("bufer %s\n", bufer_symb);
			//connect(my_sock, (struct sockaddr *) &serv_addr, sizeof (serv_addr));
            write(my_sock, bufer_symb, strlen(bufer_symb)); // отправляем серверу результат tcp
            printf("длина строки %d\n", strlen(bufer_symb));
            //close(my_sock);
        }
    }
    printf("Recv error \n");
    close(my_sock);
    return -1;
}
